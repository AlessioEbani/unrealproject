// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UnrealProject/UnrealProjectCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUnrealProjectCharacter() {}
// Cross Module References
	UNREALPROJECT_API UFunction* Z_Construct_UDelegateFunction_UnrealProject_GameStateCharacter__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_UnrealProject();
	UNREALPROJECT_API UClass* Z_Construct_UClass_AUnrealProjectCharacter_NoRegister();
	UNREALPROJECT_API UClass* Z_Construct_UClass_AUnrealProjectCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UCurveVector_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCurveFloat_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_UnrealProject_GameStateCharacter__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UnrealProject_GameStateCharacter__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UnrealProject_GameStateCharacter__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_UnrealProject, nullptr, "GameStateCharacter__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UnrealProject_GameStateCharacter__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UnrealProject_GameStateCharacter__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UnrealProject_GameStateCharacter__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UnrealProject_GameStateCharacter__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(AUnrealProjectCharacter::execHandleProgressOffset)
	{
		P_GET_STRUCT(FVector,Z_Param_offset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandleProgressOffset(Z_Param_offset);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AUnrealProjectCharacter::execHandleProgressArmLength)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_armLength);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandleProgressArmLength(Z_Param_armLength);
		P_NATIVE_END;
	}
	void AUnrealProjectCharacter::StaticRegisterNativesAUnrealProjectCharacter()
	{
		UClass* Class = AUnrealProjectCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "HandleProgressArmLength", &AUnrealProjectCharacter::execHandleProgressArmLength },
			{ "HandleProgressOffset", &AUnrealProjectCharacter::execHandleProgressOffset },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressArmLength_Statics
	{
		struct UnrealProjectCharacter_eventHandleProgressArmLength_Parms
		{
			float armLength;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_armLength;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressArmLength_Statics::NewProp_armLength = { "armLength", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UnrealProjectCharacter_eventHandleProgressArmLength_Parms, armLength), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressArmLength_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressArmLength_Statics::NewProp_armLength,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressArmLength_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressArmLength_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUnrealProjectCharacter, nullptr, "HandleProgressArmLength", nullptr, nullptr, sizeof(UnrealProjectCharacter_eventHandleProgressArmLength_Parms), Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressArmLength_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressArmLength_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressArmLength_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressArmLength_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressArmLength()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressArmLength_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressOffset_Statics
	{
		struct UnrealProjectCharacter_eventHandleProgressOffset_Parms
		{
			FVector offset;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_offset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressOffset_Statics::NewProp_offset = { "offset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UnrealProjectCharacter_eventHandleProgressOffset_Parms, offset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressOffset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressOffset_Statics::NewProp_offset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressOffset_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressOffset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUnrealProjectCharacter, nullptr, "HandleProgressOffset", nullptr, nullptr, sizeof(UnrealProjectCharacter_eventHandleProgressOffset_Parms), Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressOffset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressOffset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressOffset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressOffset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressOffset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressOffset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AUnrealProjectCharacter_NoRegister()
	{
		return AUnrealProjectCharacter::StaticClass();
	}
	struct Z_Construct_UClass_AUnrealProjectCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_aimOutBoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_aimOutBoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_aimInBoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_aimInBoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseLookUpRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseLookUpRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseTurnRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseTurnRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_offsetCurve_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_offsetCurve;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_targetArmCurve_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_targetArmCurve;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnAimOut_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnAimOut;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnAimIn_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnAimIn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_onCharacterUnCrouch_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_onCharacterUnCrouch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_onCharacterCrouch_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_onCharacterCrouch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_yaw_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_yaw;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_hasRifle_MetaData[];
#endif
		static void NewProp_hasRifle_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_hasRifle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_pitch_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_pitch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_aiming_MetaData[];
#endif
		static void NewProp_aiming_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_aiming;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FollowCamera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FollowCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraBoom;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AUnrealProjectCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_UnrealProject,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AUnrealProjectCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressArmLength, "HandleProgressArmLength" }, // 2827893858
		{ &Z_Construct_UFunction_AUnrealProjectCharacter_HandleProgressOffset, "HandleProgressOffset" }, // 494309373
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "UnrealProjectCharacter.h" },
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aimOutBoom_MetaData[] = {
		{ "Category", "Camera" },
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aimOutBoom = { "aimOutBoom", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUnrealProjectCharacter, aimOutBoom), METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aimOutBoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aimOutBoom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aimInBoom_MetaData[] = {
		{ "Category", "Camera" },
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aimInBoom = { "aimInBoom", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUnrealProjectCharacter, aimInBoom), METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aimInBoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aimInBoom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_BaseLookUpRate_MetaData[] = {
		{ "Category", "Camera" },
		{ "Comment", "/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */" },
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
		{ "ToolTip", "Base look up/down rate, in deg/sec. Other scaling may affect final rate." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_BaseLookUpRate = { "BaseLookUpRate", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUnrealProjectCharacter, BaseLookUpRate), METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_BaseLookUpRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_BaseLookUpRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_BaseTurnRate_MetaData[] = {
		{ "Category", "Camera" },
		{ "Comment", "/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */" },
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
		{ "ToolTip", "Base turn rate, in deg/sec. Other scaling may affect final turn rate." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_BaseTurnRate = { "BaseTurnRate", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUnrealProjectCharacter, BaseTurnRate), METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_BaseTurnRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_BaseTurnRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_offsetCurve_MetaData[] = {
		{ "Category", "Timeline" },
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_offsetCurve = { "offsetCurve", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUnrealProjectCharacter, offsetCurve), Z_Construct_UClass_UCurveVector_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_offsetCurve_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_offsetCurve_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_targetArmCurve_MetaData[] = {
		{ "Category", "Timeline" },
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_targetArmCurve = { "targetArmCurve", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUnrealProjectCharacter, targetArmCurve), Z_Construct_UClass_UCurveFloat_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_targetArmCurve_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_targetArmCurve_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_OnAimOut_MetaData[] = {
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_OnAimOut = { "OnAimOut", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUnrealProjectCharacter, OnAimOut), Z_Construct_UDelegateFunction_UnrealProject_GameStateCharacter__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_OnAimOut_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_OnAimOut_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_OnAimIn_MetaData[] = {
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_OnAimIn = { "OnAimIn", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUnrealProjectCharacter, OnAimIn), Z_Construct_UDelegateFunction_UnrealProject_GameStateCharacter__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_OnAimIn_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_OnAimIn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_onCharacterUnCrouch_MetaData[] = {
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_onCharacterUnCrouch = { "onCharacterUnCrouch", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUnrealProjectCharacter, onCharacterUnCrouch), Z_Construct_UDelegateFunction_UnrealProject_GameStateCharacter__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_onCharacterUnCrouch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_onCharacterUnCrouch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_onCharacterCrouch_MetaData[] = {
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_onCharacterCrouch = { "onCharacterCrouch", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUnrealProjectCharacter, onCharacterCrouch), Z_Construct_UDelegateFunction_UnrealProject_GameStateCharacter__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_onCharacterCrouch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_onCharacterCrouch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_yaw_MetaData[] = {
		{ "Category", "UnrealProjectCharacter" },
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_yaw = { "yaw", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUnrealProjectCharacter, yaw), METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_yaw_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_yaw_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_hasRifle_MetaData[] = {
		{ "Category", "UnrealProjectCharacter" },
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_hasRifle_SetBit(void* Obj)
	{
		((AUnrealProjectCharacter*)Obj)->hasRifle = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_hasRifle = { "hasRifle", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AUnrealProjectCharacter), &Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_hasRifle_SetBit, METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_hasRifle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_hasRifle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_pitch_MetaData[] = {
		{ "Category", "UnrealProjectCharacter" },
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_pitch = { "pitch", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUnrealProjectCharacter, pitch), METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_pitch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_pitch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aiming_MetaData[] = {
		{ "Category", "UnrealProjectCharacter" },
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aiming_SetBit(void* Obj)
	{
		((AUnrealProjectCharacter*)Obj)->aiming = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aiming = { "aiming", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AUnrealProjectCharacter), &Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aiming_SetBit, METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aiming_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aiming_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_FollowCamera_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Follow camera */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
		{ "ToolTip", "Follow camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_FollowCamera = { "FollowCamera", nullptr, (EPropertyFlags)0x00400000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUnrealProjectCharacter, FollowCamera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_FollowCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_FollowCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_CameraBoom_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Camera boom positioning the camera behind the character */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "UnrealProjectCharacter.h" },
		{ "ToolTip", "Camera boom positioning the camera behind the character" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_CameraBoom = { "CameraBoom", nullptr, (EPropertyFlags)0x00400000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUnrealProjectCharacter, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_CameraBoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_CameraBoom_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AUnrealProjectCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aimOutBoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aimInBoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_BaseLookUpRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_BaseTurnRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_offsetCurve,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_targetArmCurve,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_OnAimOut,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_OnAimIn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_onCharacterUnCrouch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_onCharacterCrouch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_yaw,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_hasRifle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_pitch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_aiming,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_FollowCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnrealProjectCharacter_Statics::NewProp_CameraBoom,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AUnrealProjectCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AUnrealProjectCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AUnrealProjectCharacter_Statics::ClassParams = {
		&AUnrealProjectCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AUnrealProjectCharacter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AUnrealProjectCharacter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AUnrealProjectCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AUnrealProjectCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AUnrealProjectCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AUnrealProjectCharacter, 456338525);
	template<> UNREALPROJECT_API UClass* StaticClass<AUnrealProjectCharacter>()
	{
		return AUnrealProjectCharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AUnrealProjectCharacter(Z_Construct_UClass_AUnrealProjectCharacter, &AUnrealProjectCharacter::StaticClass, TEXT("/Script/UnrealProject"), TEXT("AUnrealProjectCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AUnrealProjectCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
