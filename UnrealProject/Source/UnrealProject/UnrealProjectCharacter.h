// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/TimelineComponent.h"
#include "GameFramework/Character.h"
#include "UnrealProjectCharacter.generated.h"

using namespace std;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGameStateCharacter);

UCLASS(config=Game)
class AUnrealProjectCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite,Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
	
	/** Follow camera */
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	AUnrealProjectCharacter();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	
	UPROPERTY(BlueprintReadWrite)
	bool aiming;

	UPROPERTY(BlueprintReadOnly)
	float pitch;

	UPROPERTY(BlueprintReadWrite)
	bool hasRifle;

	UPROPERTY(BlueprintReadOnly)
	float yaw;

	UFUNCTION()
	void HandleProgressArmLength(float armLength);

	UFUNCTION()
    void HandleProgressOffset(FVector offset);
	
	UPROPERTY(BlueprintAssignable)
	FGameStateCharacter onCharacterCrouch;

	UPROPERTY(BlueprintAssignable)
	FGameStateCharacter onCharacterUnCrouch;

	UPROPERTY(BlueprintAssignable)
	FGameStateCharacter OnAimIn;

	UPROPERTY(BlueprintAssignable)
	FGameStateCharacter OnAimOut;

	UPROPERTY(EditAnywhere, Category="Timeline")
	UCurveFloat* targetArmCurve;

	UPROPERTY(EditAnywhere, Category="Timeline")
	UCurveVector* offsetCurve;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category=Camera)
	float aimInBoom;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category=Camera)
	float aimOutBoom;

	float initialPitch;
	float initialYaw;

protected:

	FTimeline aimTimeline;
	
	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	// FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	// /** Returns FollowCamera subobject **/
	// FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	void CrouchCharacter();
	void UnCrouchCharacter();
	void AimIn();
	void AimOut();
};

